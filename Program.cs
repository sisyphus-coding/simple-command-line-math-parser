﻿using System;
using System.Collections;
using System.Collections.Generic;

using org.mariuszgromada.math.mxparser;
using System.Linq;

namespace SCLMP
{
	class MainClass
	{
		public const string divisor = "<=0=>-";
		public const string endDivisor = "<=0=>";

		public static List<Argument> variables = new List<Argument> ();

		public static void Main (string[] args)
		{
			Console.WriteLine ("Input any math expression, OR\n" +
							   "Input a command to run it,\n" +
							   "You can get a list of commands by typing 'Help'\n" +
							   "Type 'Quit' to exit");

			//Loop While We Should Keep Going.
			bool keepgoing = true;
			while (keepgoing) {

				//Get What The User Types.
				string input = Console.ReadLine ().ToLower();

				//Split It By Spaces.
				List<string> div = input.Split (char.Parse(" ")).ToList();

				//Check What The First Part Is.
				switch (div [0]) {

				//If The First Part Is "Help", Give The User A List Of Avalable Commands.
				case "help":
					Console.WriteLine  ("[num] Means Any Number\n" +
										"[name] Means Any Non Spaced Name\n\n" +
										"Command Tree:\n" +
										"\n" +
										"Var Add [name]  # Adds A New Variable Named [name] With The Value 0\n" +
										"Var Add [name] [num]  # Adds A New Variable Named [name] With The Value [num]\n" +
										"Var Remove [name]  # Removes The Variable Named [name]\n" +
										"Var Change [name] [num]  # Changes The Variable Named [name] To Have The Value [num].\n" +
										"\tEqual Command To 'Set [name] [num]'\n" +
										"Var Clear  # Clears All Variables\n");
					break;




				//If The First Part Is "Var", Its A Variable Command.
				case "var":
					if (div.Count < 2) {
						continue;
					}

					switch (div [1]) {

					//If The User Types "Var Add", Add A Variable.
					case "add":
						if (div.Count < 3) {
							continue;
						}

						double num1 = 0;
						if (div.Count >= 4) {
							//Check If The 4:th Element Is A Number Or Expression.
							if (!double.TryParse (div [3], out num1)) {

								//Its Not A Number, Check If Its An Expression.
								//It Counts As An Expression If Its The Name Of A Variable.
								if (!TryGetResult(div[3], out num1, variables.ToArray())){
									//Its Not An Expression, Just Keep Going.
									continue;
								}
							}
						}

						//Create The New Argument
						Argument a = new Argument (div [2], num1);

						variables.Add (a);
						break;

						//If The User Types "Var Clear", Remove All The Variables.
					case "clear":
						variables = new List<Argument> ();
						break;

					//If The User Types "Var Remove", Remove The Specified Variable.
					case "remove":
						if (div.Count < 3) {
							continue;
						}
						foreach (Argument arg in variables) {
							if (arg.getArgumentName () == div [2]) {
								variables.Remove(arg);
								break;
							}
						}


						break;

						//The User Wants To Change A Variable
					case "change":
						if (div.Count < 4) {
							continue;
						}

						double num2;

						//Check If The 4:th Element Is A Number Or Expression.
						if (!double.TryParse (div [3], out num2)) {

							//Its Not A Number, Check If Its An Expression.
							//It Counts As An Expression If Its The Name Of A Variable.
							if (!TryGetResult(div[3], out num2, variables.ToArray())){
								//Its Not An Expression, Just Keep Going.
								continue;
							}
						}



						foreach (Argument arg in variables) {
							if (arg.getArgumentName () == div [2]) {
								arg.setArgumentValue (num2);
								break;
							}
						}
						break;

					case "list":
						string s = "";

						foreach (Argument arg in variables) {
							s += "\t" + arg.getArgumentName () + "  =  " + arg.getArgumentValue () + "\n";
						}

						Console.WriteLine  ("\n" +
											"  Listing Variables: \n" +
											"\n" +
											"{0}"
											, s);

						break;


					default:
						//The User Typed Something Wierd, Ignore It.
						continue;
					}
					break;
				
					//Shortcut To "Var Change"
				case "set":
					if (div.Count < 3) {
						continue;
					}

					double num3;

					//Check If The 4:th Element Is A Number Or Expression.
					if (!double.TryParse (div [2], out num3)) {

						//Its Not A Number, Check If Its An Expression.
						//It Counts As An Expression If Its The Name Of A Variable.
						if (!TryGetResult(div[2], out num3, variables.ToArray())){
							//Its Not An Expression, Just Keep Going.
							continue;
						}
					}



					foreach (Argument arg in variables) {
						if (arg.getArgumentName () == div [1]) {
							arg.setArgumentValue (num3);
							break;
						}
					}
					break;

				//If The User Types Quit, Quit The Application.
				case "quit":
					keepgoing = false;
					return;

				default:
					//The Input Is Not A Command, Then It Is An Expression. 
					//So We Calculate It.
					PrintResult (input, variables.ToArray ());
					break;
				}



			}
		}


		public static void PrintResult(Expression expression){
			double result = expression.calculate ();
			int num = result.ToString ().Length;
			string s = "";

			num /= divisor.Length;

			for (int i = 0; i < num; i++) {
				s += divisor;
			}
			s += endDivisor;

			if (result.ToString() != "¤¤¤"){
				Console.WriteLine (" =\n{0}\n{1}", result.ToString(), s);
			}
		}

		public static void PrintResult(string expressionString){
			Expression e = new Expression (expressionString);
			PrintResult (e);
		}

		public static void PrintResult(string expressionString, params Argument[] args){
			Expression e = new Expression (expressionString, args);
			PrintResult (e);
		}


		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


		public static double GetResult(Expression expression){
			return expression.calculate ();
		}

		public static double GetResult(string expressionString){
			Expression e = new Expression (expressionString);
			return GetResult (e);
		}

		public static double GetResult(string expressionString, params Argument[] args){
			Expression e = new Expression (expressionString, args);
			return GetResult (e);
		}


		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
		//|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||


		public static bool TryGetResult(Expression expression, out double result){
			result = expression.calculate ();

			if (result.ToString () != "¤¤¤") {
				return true;
			} else
				return false;
		}

		public static bool TryGetResult(string expressionString, out double result){
			Expression e = new Expression (expressionString);
			return TryGetResult (e, out result);
		}

		public static bool TryGetResult(string expressionString, out double result, params Argument[] args){
			Expression e = new Expression (expressionString, args);
			return TryGetResult (e, out result);
		}
	}
}
